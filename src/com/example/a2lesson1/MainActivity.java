package com.example.a2lesson1;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;


public class MainActivity extends FragmentActivity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);        
        
    }
	
	public void loadtextfragment(String t) {
		Bundle data = new Bundle();
		data.putString("text",t);
	    FragmentManager fm = getSupportFragmentManager();
	    FragmentTransaction ft = fm.beginTransaction();
	    textfragment tf = new textfragment();
	    tf.setArguments(data);
	    ft.add(R.id.fragcontainer, tf);
	    ft.commit();
	  }

}
