package com.example.a2lesson1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class editfragment extends Fragment{
	private Button b;
	private EditText et;
	 	@Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
//	        // Inflate the layout for this fragment		 
	         View view = inflater.inflate(R.layout.editfragment, container, false);	         
	         et = (EditText) view.findViewById(R.id.edittext);
	         b = (Button) view.findViewById(R.id.swap);
	         b.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					MainActivity activity = (MainActivity) getActivity();
				     activity.loadtextfragment(et.getText().toString());
				}
	        	 
	         });
	        
	         return view;
	         
	    }
	 
	 

	 

}
