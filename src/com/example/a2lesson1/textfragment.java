package com.example.a2lesson1;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class textfragment  extends Fragment{

	private TextView t;
	private Button b;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.textfragment, container, false);
        TextView tx = (TextView) v.findViewById(R.id.textView);
        Bundle b = this.getArguments();
        String t = b.getString("text");
        if (t != null)
        	tx.setText(t);
        return v;
    }
}
